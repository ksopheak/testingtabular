import { Tabular } from 'meteor/aldeed:tabular';
import { Books } from './data.js';

export const TabularTables = {};

TabularTables.Books = new Tabular.Table({
        name: "Books",
        collection: Books,
        responsive: true,
        bFilter: true,
        stateSave: true,
        autoWidth: false,
        columns: [

                {data: 'fname', title: 'fname' },
                {data: 'lname', title: 'lname' },
                {data: 'age', title: 'age' },
                {
                        data: "_id", width:'200px', orderable: false, className: "text-center",
                        title: "Action",
                        render: function (e){
                                var btntxt = new Spacebars.SafeString("<a href='' id='edit_asset' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Change</a> ");
                                return btntxt + new Spacebars.SafeString("<a href='' id='delete_asset' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>");
                        }
                }
        ]
});
