import SimpleSchema from 'simpl-schema';
import { Mongo } from 'meteor/mongo';

export const Books = new Mongo.Collection("books");

const Schemas = {};

Schemas.Book = new SimpleSchema({
    fname: {
        type: String,
        label: "fname",
        max: 200
    },
    lname: {
        type: String,
        label: "lname",
        max: 200
    },
    age: {
        type:  SimpleSchema.Integer,
        label: "age",
        optional: true,
    }

});
Books.attachSchema(Schemas.Book);