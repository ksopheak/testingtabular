import { Meteor } from 'meteor/meteor';
import { Books } from '../collections/data.js';
import '../collections/tabular.js';

Meteor.startup(() => {
  // code to run on server at startup
  if (Books.find().count() === 0) {
    [
      {fname: "Kheng", lname: "Sopheak", age: 29},
      {fname: "Houn", lname: "Darith", age: 35},
      {fname: "Norng", lname: "Saoleng", age: 55},
      {fname: "Kean", lname: "Meyly", age: 25},
      {fname: "Chea", lname: "Vanny", age: 27},
      {fname: "Lam", lname: "Chanthida", age: 29},
      {fname: "Som", lname: "Sophal", age: 35},
      {fname: "Boo", lname: "Ratanak", age: 29},
      {fname: "Duk", lname: "Pharun", age: 25},
      {fname: "Kim", lname: "Lyhour", age: 35},
      {fname: "Tem", lname: "Thida", age: 29},
    ].forEach(function(books){
      Books.insert(books);
    });
  }
});
